
FROM alpine:3.14.3

# author
MAINTAINER peacemaker

RUN echo "now building..."

ARG DOCKER_ENV
ARG DOCKER_COMPOSE_PROJECT_NAME
ARG NGINX_SERVER_NAME

RUN apk  --update add tzdata && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

RUN apk add openrc
RUN sed -i'.bak' 's/^#rc_sys=""/rc_sys="lxc"/' /etc/rc.conf
RUN sed -i 's/^#rc_provide="!net"/rc_provide="loopback net"/' /etc/rc.conf
RUN sed -i'.bak' '/getty/d' /etc/inittab
RUN sed -i'.bak' 's/mount -t tmpfs/# mount -t tmpfs/' /lib/rc/sh/init.sh
RUN sed -i'.bak' 's/hostname $opts/# hostname $opts/' /etc/init.d/hostname
RUN sed -i'.bak' 's/cgroup_add_service$/# cgroup_add_service/' /lib/rc/sh/openrc-run.sh
RUN mkdir -p /run/openrc
RUN touch /run/openrc/softlevel

RUN apk add util-linux \
  bind-tools \
  curl \
  git \
  wget

RUN apk add nginx

RUN apk add php7 \
  php7-fpm \
  php7-opcache \
  php7-curl \
  php7-json \
  php7-iconv \
  php7-openssl \
  php7-mbstring \
  php7-phar \
  php7-dom \
  php7-xml \
  php7-xmlwriter \
  php7-tokenizer

RUN apk add php7-pdo_mysql \
  php7-simplexml

RUN apk add mysql-client

RUN rm -rf /var/cache/apk/*

RUN /sbin/rc-update add nginx
RUN /sbin/rc-update add php-fpm7

COPY ./etc /etc
COPY ./var /var

RUN sed -i'.bak' 's/^expose_php = On$/expose_php = Off/' /etc/php7/php.ini

RUN addgroup -S php-fpm
RUN adduser -S -G php-fpm -g php-fpm -s /sbin/nologin -D -H -h /var/lib/php7/fpm php-fpm
RUN sed -i'.bak' 's/user = nobody$/user = php-fpm/' /etc/php7/php-fpm.d/www.conf
RUN sed -i 's/group = nobody$/group = php-fpm/' /etc/php7/php-fpm.d/www.conf

RUN addgroup nginx php-fpm
RUN sed -i 's/^listen = 127.0.0.1:9000$/listen = \/var\/run\/php-fpm7\/php-fpm.sock/' /etc/php7/php-fpm.d/www.conf
RUN sed -i 's/^;listen.owner = nobody$/listen.owner = php-fpm/' /etc/php7/php-fpm.d/www.conf
RUN sed -i 's/^;listen.group = php-fpm$/listen.group = php-fpm/' /etc/php7/php-fpm.d/www.conf
RUN sed -i 's/^;listen.mode = 0660$/listen.mode = 0660/' /etc/php7/php-fpm.d/www.conf
RUN sed -i'.bak' 's/fastcgi_pass   127.0.0.1:9000;/fastcgi_pass   unix:\/var\/run\/php-fpm7\/php-fpm.sock;/' /etc/nginx/conf.d/default.conf
RUN sed -i "s/server_name  localhost;/server_name  $NGINX_SERVER_NAME;/" /etc/nginx/conf.d/default.conf

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

CMD ["/sbin/init"]
