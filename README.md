### 注意事項 ###
* `php.ini` の `date.timezone` がデフォルトの UTC になっている。
    * php の timezone を変更する場合は `/docker/nginx/Dockerfile` に `RUN sed -i 's/^;date.timezone =$/date.timezone = Asia\/Tokyo/' /etc/php7/php.ini` を追加する必要がある。

### docker-compose の 設定確認 コマンド ###
`APP_HOST_PORT=8080 DB_HOST_PORT=33060 docker-compose --env-file=./docker/env/local.compose.env -f ./docker/docker-compose-app.yml -f ./docker/docker-compose-db.yml -f ./docker/docker-compose-network.yml config`

### docker-compose の build と 起動 コマンド ###
`APP_HOST_PORT=8080 DB_HOST_PORT=33060 docker-compose --env-file=./docker/env/local.compose.env -f ./docker/docker-compose-app.yml -f ./docker/docker-compose-db.yml -f ./docker/docker-compose-network.yml up --build -d`

### container / volume / network を削除 コマンド ###
`APP_HOST_PORT=8080 DB_HOST_PORT=33060 docker-compose --env-file=./docker/env/local.compose.env -f ./docker/docker-compose-app.yml -f ./docker/docker-compose-db.yml -f ./docker/docker-compose-network.yml down --volumes`

### container の image を含めたすべてを削除 コマンド ###
`APP_HOST_PORT=8080 DB_HOST_PORT=33060 docker-compose --env-file=./docker/env/local.compose.env -f ./docker/docker-compose-app.yml -f ./docker/docker-compose-db.yml -f ./docker/docker-compose-network.yml down --rmi all --volumes`

### docker 各種確認 コマンド ###
`docker ps`
`docker ps -a`
`docker images`
`docker network ls`
`docker volume ls`

### app container 接続 コマンド ###
`docker exec -it local-$(basename $(pwd)).app ash --login`

### db container 接続 コマンド ###
`docker exec -it local-$(basename $(pwd)).db bash --login`


### 環境 ###
`docker --version`
Docker version 19.03.8, build afacb8b

`docker-compose --version`
docker-compose version 1.25.4, build 8d51620a
